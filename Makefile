# Invoke Runway

test:
	@echo "Test Successful!"
	# pipenv run runway test

plan:
	pipenv run runway plan

prepare:
	@pushd bootstrap.cfn && \
	DEPLOY_ENVIRONMENT=common pipenv run stacker build -t *.env 01-bootstrap.yaml -r us-west-2

deploy:
	DEPLOY_ENVIRONMENT=common pipenv run runway deploy

destroy:
	DEPLOY_ENVIRONMENT=common pipenv run runway destroy

destroy-vpc:
	@pushd core.cfn && \
	@stacker destroy --force -t *.env core.yaml

verify:
	@echo "Test Successful!"
