# msp-sample-client-repo

## Steps for use

This repo is here to help you learn both Runway and how it handles deployments of cloudformation into AWS via stacker. This repo is configured using the `Git branches` environment and file structure. This repo only works with the `common` environment, of which the `master` git branch is tied to.

### Step 1 - Prep your env

1. You must first edit your "customer" name in every environment file to be unique. Stacker (which is the tool being executed by runway), uploads your cloudformation to S3 under a S3 bucket it creates named `stacker-<customer>-<env>`. As a result, everyone *MUST* change the customer name in every environment file before proceeding.
Files to change are:
* bootstracp.cfn/common-us-west-2.env
* core.cfn/common-us-west-2.env
* sns-topics.cfn/common-us-west-2.env
* aws-health-events-to-slack.cfn/common-us-west-2.env

2. Install pipenv - `brew install pipenv` - Pipenv automatically creates and manages a virtualenv for this project (and all projects), as well as adds/removes packages from your Pipfile as you install/uninstall packages. It also generates the ever-important Pipfile.lock, which is used to produce deterministic builds.

3. Install the Chef DK - this is needed by one of the modules (the bootstrap.cfn one has a hook)

4. Execute `make prepare` - this will prep your account, setup some S3 buckets and an EIP for use by the VPN

### Step 2 - Deploy your stacks

After you have prepped your environment, you may proceed with a runway deployment.

`make deploy`

The above command will execute `runway deploy` in a python virtual environment.

### Tear Down / The "I want it to go away" section

Runway can tear down/destory your whole stack.  Runway allows for you to do this via the cli, that we have prepackaged a MakeFile that does this for you:

`make destroy`

Note: the sns-topics module will fail if the topic has a pending subscription. This is normal. 

## NOTE
NOTE: Master branch can only be written by Pod-Leads. If you find an error with this repo, make sure you create your change in another branch (that is NOT named starting with a `ENV-`), then submit a Pull Request.
