""" Load dependencies """
from __future__ import print_function

from __init__ import version  # pylint: disable=W0403
from troposphere import Ref, Join, Sub, Output, awslambda, iam, Export, GetAtt, events, Not, Equals, cloudwatch, If

import awacs.s3
import awacs.awslambda
import awacs.logs
import awacs.sqs
import awacs.sns
import awacs.sts
from awacs.aws import Allow, Statement, Policy, Principal

from stacker.blueprints.variables.types import CFNString
from stacker.blueprints.base import Blueprint


class BlueprintClass(Blueprint):
    """Blueprint for creating lambda function."""

    VARIABLES = {
        'ApplicationName': {
            'type': CFNString,
            'description': 'Name of lambda',
            'default': 'health-events-notifier'
        },
        'CustomerName': {
            'type': CFNString,
            'description': 'The Customer Name'
        },
        'EnvironmentName': {
            'type': CFNString,
            'description': 'The Environment Name',
            'default': 'common'
        },
        'LambdaHandler': {
            'type': CFNString,
            'description': 'Lambda entrypoint',
            'default': 'index.handler'
        },
        'LambdaDescription': {
            'type': CFNString,
            'description': '',
            'default': 'AWS Health Events Notification Lambda'
        },
        'FunctionMemorySize': {
            'type': CFNString,
            'description': 'How much memory do you want to allocate to this'
                           ' function?',
            'default': '256'
        },
        'FunctionRuntime': {
            'type': CFNString,
            'description': 'Which runtime should be used for the function?',
            'allowed_values': ['nodejs4.3', 'nodejs6.10', 'python2.7'],
            'default': 'python2.7'
        },
        'FunctionTimeout': {
            'type': CFNString,
            'description': 'How long should the function be allowed to run',
            'default': '60'
        },
        'CodeS3Bucket': {
            'type': CFNString,
            'description': 'CodeS3Bucket'
        },
        'CodeS3Key': {
            'type': CFNString,
            'description': 'CodeS3Key'
        },
        'SlackHook': {
            'type': CFNString,
            'description': 'IncomingWebHookURL'
        },
        'SlackChannel': {
            'type': CFNString,
            'description': 'SlackChannel'
        },
        'AlertTopicArn': {
            'type': CFNString,
            'default': '',
            'description': 'Arn of the SNS alert topic to send alarms to',
        },
        'AlertInsuffTopicArn': {
            'type': CFNString,
            'default': '',
            'description': 'Arn of the SNS topic to send alarms to when '
                           'insufficient',
        },
        'AlarmThreshold': {
            'type': CFNString,
            'default': '0',
            'description': 'Expression for the rule to be scheduled',
        },
        'AlarmPeriod': {
            'type': CFNString,
            'default': '60',
            'description': 'How many minutes in the sample period',
        },
        'AlarmPeriodCount': {
            'type': CFNString,
            'default': '5',
            'description': 'How many periods in the sample',
        },
    }

    def add_conditions(self):
        """Set up template conditions."""
        template = self.template
        variables = self.get_variables()

        template.add_condition(
            'HasSNSAlertTopic',
            Not(Equals(variables['AlertTopicArn'].ref, ''))
        )
        template.add_condition(
            'HasSNSInsufficientTopic',
            Not(Equals(variables['AlertInsuffTopicArn'].ref, ''))
        )

    def create_resources(self):
        """Create the resources."""
        template = self.template
        variables = self.get_variables()

        lambda_iam_role = template.add_resource(
            iam.Role(
                'LambdaRole',
                RoleName=Join('-',
                              [variables['CustomerName'].ref,
                               variables['ApplicationName'].ref,
                               variables['EnvironmentName'].ref,
                               'lambda-role']),
                AssumeRolePolicyDocument=Policy(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service',
                                                ['lambda.amazonaws.com'])
                        )
                    ]
                ),
                Path='/service-role/',
                ManagedPolicyArns=[
                    'arn:aws:iam::aws:policy/'
                    'AWSXrayWriteOnlyAccess',
                    'arn:aws:iam::aws:policy/service-role/'
                    'AWSLambdaBasicExecutionRole',
                    'arn:aws:iam::aws:policy/service-role/'
                    'AWSLambdaVPCAccessExecutionRole'
                ],
                Policies=[
                    iam.Policy(
                        PolicyName=Join('-',
                                        [variables['CustomerName'].ref,
                                         variables['ApplicationName'].ref,
                                         variables['EnvironmentName'].ref,
                                         'lambda-policy']),
                        PolicyDocument=Policy(
                            Version='2012-10-17',
                            Statement=[
                                Statement(
                                    Action=[
                                        awacs.logs.CreateLogGroup,
                                        awacs.logs.CreateLogStream,
                                        awacs.logs.PutLogEvents
                                    ],
                                    Effect=Allow,
                                    Resource=['arn:aws:logs:*:*:*'],
                                    Sid='WriteLogs'
                                )
                            ]
                        )
                    )
                ]
            )
        )

        env_vars = awslambda.Environment(
            Variables={
                'SLACK_CHANNEL': variables['SlackChannel'].ref,
                'SLACK_HOOK': variables['SlackHook'].ref
            }
        )

        lambda_function = template.add_resource(
            awslambda.Function(
                'LambdaFunction',
                Description=variables['LambdaDescription'].ref,
                Code=awslambda.Code(
                    S3Bucket=variables['CodeS3Bucket'].ref,
                    S3Key=variables['CodeS3Key'].ref
                ),
                Environment=env_vars,
                Handler=variables['LambdaHandler'].ref,
                Role=GetAtt(lambda_iam_role, 'Arn'),
                Runtime=variables['FunctionRuntime'].ref,
                Timeout=variables['FunctionTimeout'].ref,
                MemorySize=variables['FunctionMemorySize'].ref,
                FunctionName=Join('-',
                                  [variables['CustomerName'].ref,
                                   variables['ApplicationName'].ref,
                                   'lambda',
                                   variables['EnvironmentName'].ref]),
                # TracingConfig='Active'
            )
        )

        events_rule = template.add_resource(
            events.Rule(
                'CloudWatchEventRule',
                Description='Cloudwatch Event Rule',
                State='ENABLED',
                Name=Join('-',
                          [variables['CustomerName'].ref,
                           variables['ApplicationName'].ref,
                           variables['EnvironmentName'].ref,
                           'event-rule']),
                EventPattern={
                    "source": ["aws.health"]
                },
                Targets=[
                    events.Target(
                        Arn=GetAtt(lambda_function, 'Arn'),
                        Id='LambdaFn'
                    )
                ]
            )
        )

        alarm = template.add_resource(  # pylint: disable=W0612
            cloudwatch.Alarm(
                'LambdaErrorAlarm',
                AlarmName=Join('-', [Ref(lambda_function),
                                     'lambda-exec-err']),
                AlarmDescription='Lambda Execution Errors',
                Namespace='AWS/Lambda',
                Statistic='Minimum',
                Period=variables['AlarmPeriod'].ref,
                EvaluationPeriods=variables['AlarmPeriodCount'].ref,
                Threshold=variables['AlarmThreshold'].ref,
                AlarmActions=If('HasSNSAlertTopic',
                                [variables['AlertTopicArn'].ref],
                                [Ref('AWS::NoValue')]),
                OKActions=If('HasSNSAlertTopic',
                             [variables['AlertTopicArn'].ref],
                             [Ref('AWS::NoValue')]),
                InsufficientDataActions=If('HasSNSAlertTopic',
                                           [variables['AlertTopicArn'].ref],
                                           [Ref('AWS::NoValue')]),
                ComparisonOperator='GreaterThanThreshold',
                Dimensions=[cloudwatch.MetricDimension(
                    Name='FunctionName',
                    Value=lambda_function.title                    
                )],
                MetricName='Errors',
                TreatMissingData='notBreaching'
            )
        )

        template.add_resource(
            awslambda.Permission(
                'LambdaInvokePermission',
                FunctionName=Ref(lambda_function),
                Action='lambda:InvokeFunction',
                Principal='events.amazonaws.com',
                SourceArn=GetAtt(events_rule, 'Arn')
            )
        )

        template.add_output(
            Output(
                lambda_iam_role.title,
                Description='Lambda Role',
                Export=Export(Sub('${AWS::StackName}-%s' % lambda_iam_role.title)),  # nopep8 pylint: disable=C0301
                Value=Ref(lambda_iam_role)
            )
        )
        template.add_output(
            Output(
                lambda_function.title,
                Description='Lambda Function',
                Export=Export(Sub('${AWS::StackName}-%s' % lambda_function.title)),  # nopep8 pylint: disable=C0301
                Value=GetAtt(lambda_function, 'Arn')
            )
        )
        template.add_output(
            Output(
                lambda_function.title + 'Name',
                Description='Lambda Function Name',
                Export=Export(Sub('${AWS::StackName}-%sName' % lambda_function.title)),  # nopep8 pylint: disable=C0301
                Value=Ref(lambda_function)
            )
        )

    def create_template(self):
        self.template.add_version('2010-09-09')
        self.template.add_description('Onica AWS Health Events Notifier'
                                      ' - {0}'.format(version()))
        self.add_conditions()
        self.create_resources()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context
    from stacker.variables import Variable

    BLUEPRINT = BlueprintClass("test",
                               Context({"namespace": "test"}),
                               None)
    # Check the blueprint for CFN parameters in its variables, and define bogus
    # values for those parameters so the template can be generated.
    # Easiest check to find variables that are CFN parameters (and not native
    # python types) seems to be looking for the 'parameter_type' attribute
    TEST_VARIABLES = []
    for key, value in BLUEPRINT.defined_variables().iteritems():
        if hasattr(value['type'], 'parameter_type'):
            TEST_VARIABLES.append(Variable(key, 'go_sturdy'))

    BLUEPRINT.resolve_variables(TEST_VARIABLES)
    print(BLUEPRINT.render_template()[1])
