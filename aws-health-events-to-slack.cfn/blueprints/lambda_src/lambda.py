"""Lambda for relaying cloudwatch events."""
# Sample Lambda Function to post notifications to a slack channel when an AWS Health event happens
from __future__ import print_function
# import boto3
import json
import logging
import os
from urllib2 import Request, urlopen, URLError, HTTPError

# Setting up logging
SLACK_CHANNEL = os.environ('SLACK_CHANNEL')
SLACK_HOOK = os.environ('SLACK_HOOK')
LOGGER = logging.getlogger()
LOGGER.setLevel(logging.INFO)


# main function
def handler(event, context):
    """Lambda Entrypoint."""
    message = str(event['detail']['eventDescription'][0]['latestDescription'] + \
                  "\n\n<https://phd.aws.amazon.com/phd/home?region=us-east-1#/event-log?eventID=" + \
                  event['detail']['eventArn'] + "|Click here> for details.")

    json.dumps(message)

    slack_message = {
        'channel': SLACK_CHANNEL,
        'text': message
    }

    LOGGER.info(str(slack_message))
    req = Request(SLACK_HOOK, json.dumps(slack_message))

    try:
        response = urlopen(req)
        response.read()
        LOGGER.info("Message posted to %s", slack_message['channel'])
    except HTTPError as e:
        LOGGER.error("Request failed : %d %s", e.code, e.reason)
    except URLError as e:
        LOGGER.error("Server connection failed: %s", e.reason)
